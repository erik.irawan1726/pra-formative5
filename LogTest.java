import java.util.logging.*;
import java.util.Scanner;

  public class LogTest {
      public static void main(String[] args) {
          Logger logger = Logger.getLogger(LogTest.class.getName());
          Scanner scanner = new Scanner(System.in);
          String a = scanner.nextLine();
          String b = scanner.nextLine();
          double x = 0.0;
          double y = 0.0;
          try {
               x = Integer.parseInt(a);
               y = Integer.parseInt(b);
          } catch (NumberFormatException e) {
              System.out.println("Error: "+e);
              logger.severe("Hello Severe" + LogTest.class.getName());
          }
          bagi(x,y);
          logger.info("Hello INFO " + LogTest.class.getName());
      }
      public static void bagi (double a, double b) {
         System.out.println("Hasil: :"+a/b);
      }
  }
