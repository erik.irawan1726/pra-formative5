class HelloThread extends Thread {
    @Override
    public void run() {

        String helloMsg = String.format("Thread %s is running", getId());
        System.out.println(helloMsg);
    }
    public static void main(String[] args) {
        // write your code here
        Thread t1 = new HelloThread();
        Thread t2 = new HelloThread();
        Thread t3 = new HelloThread();
        Thread t4 = new HelloThread();
        Thread t5 = new HelloThread();
        Thread t6 = new HelloThread();
        t1.run();
        t2.run();
        t3.run();
        t4.run();
        t5.run();
        t6.run();

    }
}